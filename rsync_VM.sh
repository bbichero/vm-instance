#!/bin/bash

scp root@<ip-vm>:/var/log/rsync.lite.log /root/
ssh root@<ip-vm> "rm /var/log/rsync.lite.log"

recipients="example@domain.com"
subject="42 VM rsync log : $(date +%Y_%m_%d)"
email_message="/root/rsync.lite.log"

/usr/bin/mail -s "$subject" "$recipients" < $email_message
rm /root/rsync.lite.log
