#!/bin/bash
# RSA KEY TO RELOAD

# Check if library exist in current path
if [ -f "../shell_library/type_functions.sh" ] && [ -f "../shell_library/system_functions.sh" ]
then
    # Include library
    . ../shell_library/type_functions.sh
    . ../shell_library/system_functions.sh
else
    echo "Can't load library files, abort"
    exit 1
fi

# Trap CTRL C adn execute gracefulExit when catch it
trap gracefulExit INT

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

# Define default configuration
LVM_PATH=/home/bbichero
USERNAME=bbichero
IP_REBOND_VM=
DEFAULT_VCPU=1
DEFAULT_RAM=1024
DEFAULT_DISK_SIZE=20
VM_MODELE=vm_modele
VM_MODELE_PATH=$LVM_PATH/vg0/vm-modele-disk

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -d|--debug)
            DEBUG=1
	        break
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
		# All other arg
		DEBUG=0
	    break
 	    ;;
    esac
done

if [ $DEBUG -eq "1" ]
then
	echo -e "${GRE}IP Host 2: "$IP_HOST_2
	echo "IP VM mail: "$IP_VM_MAIL
	echo "Default VCPU number: "$DEFAULT_VCPU
	echo -e "Default disk size: "$DEFAULT_DISK_SIZE${NC}
	echo
fi

function    is_network()
{
	if [ ${#1} -eq 0 ]
	then
		echo "No network type was provided"
		echo "Only dhcp or static is posible"
		return 0
	elif [ "$1" = "static" ];
	then
		return 1
	elif [ "$1" = "dhcp" ];
	then
		return 1
	else
		echo "Only dhcp or static is posible"
		return 0
	fi
}

function    ram_size()
{
	if [ $DEBUG -eq 1 ]
	then
		echo "## ENTER IN ram_size function test of $1 value ##"
	fi
	if [ "$1" -ne 256 -a "$1" -ne 512 -a "$1" -ne 1024  -a "$1" -ne 2048 -a "$1" -ne 4096 ]
	then
    		echo -e "${RED}Error: Bad RAM size given, possible values: 256, 512, 1024, 2048, 4096${NC}"
		return 0
	else
    		return 1
	fi
}

function    vcpu_number()
{
	if [ $DEBUG -eq 1 ]
	then
		echo "## ENTER IN vcpu_number function test of $1 value ##"
	fi
	if [ "$1" -gt 4 -a "$1" -lt 0 ]
	then
    		echo -e "${RED}Error: Bad number of VCPU.${NC}"
		return 0
	else
    		return 1
	fi
}

function    ft_is_ipv4()
{
	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo "Enter in ft_is_ipv4() with '`echo "$1"`' value"
    fi

	# IPv6 check
	#if [[ $s =~ ^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$ ]]; then
	if [[ $1 =~ ^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$ ]]
	then
    		return 1
	else
		if [ "$2" = "gateway" ];
		then
    			echo -e "${RED}Error: Bad gateway given.${NC}";
		elif [ "$2" = "broadcast" ];
		then
    			echo -e "${RED}Error: Bad broadcast given.${NC}";
		else
    			echo -e "${RED}Error: Bad IPv4 given.${NC}";
		fi
		return 0;
	fi
}

# Ask vm name
read -p 'VM name: ' VM_NAME
while [ ${#VM_NAME} -eq 0 ]
do
	echo "${RED}VM NAME must be not NULL${NC}"
	read -p 'VM name: ' VM_NAME
done
if [ $DEBUG -eq 1 ]
then
	echo "## V_NAME value: $VM_NAME  ##"
fi

# Ask disk name
read -p 'LVM disk name: ' DISK_NAME
while [ ${#DISK_NAME} -eq 0 ]
do
	echo "${RED}DISK NAME must be not NULL${NC}"
	read -p 'LVM disk name: ' DISK_NAME
done
if [ $DEBUG -eq 1 ]
then
	echo "## DISK_NAME value: $DISK_NAME  ##"
fi

# Ask group disk
read -p 'LVM group name: ' GROUP_DISK
while [ ${#GROUP_DISK} -eq 0 ]
do
	echo "${RED}GROUP DISK must be not NULL${NC}"
	read -p 'LVM group name: ' GROUP_DISK
done
if [ $DEBUG -eq 1 ]
then
	echo "## GROUP_DISK value: $GROUP_DISK  ##"
fi
# Ask size disk
read -p "LVM $DISK_NAME size (Default $DEFAULT_DISK_SIZE G) : " DISK_SIZE

# If disk size is not integer, ask again
if [ ${#DISK_SIZE} -eq 0 ]
then
	DISK_SIZE=$DEFAULT_DISK_SIZE
else
	while is_int "$DISK_SIZE"
	do
		read -p "LVM $DISK_NAME size: " DISK_SIZE
	done
fi
IMG_SIZE=$(($DISK_SIZE - 1))
if [ $DEBUG -eq 1 ]
then
	echo "## DISK_SIZE value: $DISK_SIZE  ##"
fi

# Create LVM disk
LVM_DISK_CREATE="sudo lvcreate -L $DISK_SIZE""G -n $DISK_NAME $GROUP_DISK"
echo -e ${YEL}$LVM_DISK_CREATE ${NC}
eval $LVM_DISK_CREATE

# Ask directory name, default /home/user/group-name/disk-name
read -p "VM path img (Default $LVM_PATH/$GROUP_DISK/$DISK_NAME): " DISK_PATH

if [ ${#DISK_PATH} -eq 0 ]
then
	DISK_PATH="$LVM_PATH/$GROUP_DISK/$DISK_NAME"
else
	while ! ft_exist_dir "$DISK_PATH"
	do
		read -p 'VM path img: ' DISK_PATH
	done
fi
if [ $DEBUG -eq 1 ]
then
	echo "## DISK_PATH value: $DISK_PATH  ##"
fi

# Ask VCPU number
read -p "VCPU number (Default $DEFAULT_VCPU): " VCPU_NUMBER

if [ ${#VCPU_NUMBER} -eq 0 ]
then
	VCPU_NUMBER=$DEFAULT_VCPU
else
	while vcpu_number "$VCPU_NUMBER"
	do
		read -p 'VCPU number: ' VCPU_NUMBER
	done
fi
if [ $DEBUG -eq 1 ]
then
	echo "## VCPU_NUMBER value: $VCPU_NUMBER  ##"
fi

# Ask RAM size
read -p "RAM size (Default $DEFAULT_RAM): " RAM_SIZE

if [ ${#RAM_SIZE} -eq 0 ]
then
	RAM_SIZE=$DEFAULT_RAM
else
	while ram_size "$RAM_SIZE"
	do
		read -p 'RAM size: ' VCPU_NUMBER
	done
fi
if [ $DEBUG -eq 1 ]
then
	echo "## RAM_SIZE value: $RAM_SIZE  ##"
fi

# Create directory for LVM disk
LVM_DISK_PATH="mkdir $DISK_PATH"
echo -e ${YEL}$LVM_DISK_PATH${NC}
eval $LVM_DISK_PATH

# Ask network configuration, static or dhc

read -p "Network configuration type : " NETWORK_TYPE

while is_network "$NETWORK_TYPE"
do
	read -p "Network configuration type : " NETWORK_TYPE
done

if [ "$NETWORK_TYPE" = "static" ]
then
	# Ask IP VM master
	read -p "$VM_NAME IPv4 master : " IPv4_M

	while ft_is_ipv4 "$IPv4_M"
	do
		read -p "$VM_NAME IPv4 master : " IPv4_M
	done
	if [ $DEBUG -eq 1 ]
	then
		echo "##  IPv4 master : $IPv4_M  ##"
	fi

	# Ask MAC addr VM master
	read -p "$VM_NAME MAC addr master : " MAC_ADDR

	while ft_is_mac_addr "$MAC_ADDR"
	do
		read -p "$VM_NAME MAC addr master : " MAC_ADDR
	done
	if [ $DEBUG -eq 1 ]
	then
		echo "##  MAC addr master: $MAC_ADDR  ##"
	fi

	# Ask gateway VM master
	read -p "$VM_NAME gateway master : " GATEWAY_M

	while ft_is_ipv4 "$GATEWAY_M" "gateway"
	do
		read -p "$VM_NAME gateway master : " GATEWAY_M
	done
	if [ $DEBUG -eq 1 ]
	then
		echo "##  GATEWAY master : $GATEWAY_M  ##"
	fi

	# Ask broadcast VM master
	read -p "$VM_NAME broadcast master : " BROADCAST_M
	while ft_is_ipv4 "$BROADCAST_M" "broadcast"
	do
		read -p "$VM_NAME broadcast master : " BROADCAST_M
	done
	if [ $DEBUG -eq 1 ]
	then
		echo "##  BROADCAST master : $BROADCAST_M  ##"
	fi
fi

# Format disk and add file system
LVM_DISK_FORMAT="sudo mkfs -t ext4 /dev/$GROUP_DISK/$DISK_NAME"
echo -e ${YEL}$LVM_DISK_FORMAT${NC}
eval $LVM_DISK_FORMAT

# Mount disk
LVM_DISK_MOUNT="sudo mount -t ext4 /dev/$GROUP_DISK/$DISK_NAME $LVM_PATH/$GROUP_DISK/$DISK_NAME"
echo -e ${YEL}$LVM_DISK_MOUNT${NC}
eval $LVM_DISK_MOUNT

# Add disk path to /etc/fstab
ADD_DISK_FSTAB=sudo echo "/dev/$GROUP_DISK/$DISK_NAME $LVM_PATH/$GROUP_DISK/$DISK_NAME ext4 defaults 0 1" | sudo tee --append /etc/fstab
echo -e ${YEL}$ADD_DISK_FSTAB${NC}
eval $ADD_DISK_FSTAB

# Clone disk and expand (resize disk) if Disk space wanted if bigger than disk vm_modele disk size
echo -e ${YEL}sudo truncate -s "$DISK_SIZE"G $DISK_PATH/"$VM_NAME".img${NC}
sudo truncate -s "$DISK_SIZE"G $DISK_PATH/"$VM_NAME".img
echo -e ${YEL}sudo chown ${USERNAME}:${USERNAME} $DISK_PATH${NC}
sudo chown ${USERNAME}:${USERNAME} $DISK_PATH

# Copy VM modele disk to new path for the new VM, and resize it if new size is greater than vm_modele VM
echo -e ${YEL}sudo virt-resize $VM_MODELE_PATH/"$VM_MODELE".img $DISK_PATH/"$VM_NAME".img${NC}
sudo virt-resize $VM_MODELE_PATH/"$VM_MODELE".img $DISK_PATH/"$VM_NAME".img

# Start pool
echo -e ${YEL}sudo virsh pool-start $DISK_NAME${NC}
sudo virsh pool-start $DISK_NAME

# If network type = dhcp, bridge is virbr0 and no mas addr must be prodive

if [ "$NETWORK_TYPE" = "static" ]
then
	echo -e "${YEL}sudo virt-install --import \
				--name $VM_NAME \
				--ram $RAM_SIZE \
                   		--vcpus $VCPU_NUMBER \
				--disk path=$DISK_PATH/"$VM_NAME".img \
				--os-type linux \
				--os-variant generic \
				--network bridge=br0,mac=$MAC_ADDR,model=virtio \
				--nographics${NC}"
	sudo virt-install       --import \
				--name $VM_NAME \
				--ram $RAM_SIZE \
                	   	--vcpus $VCPU_NUMBER \
				--disk path="$DISK_PATH"/"$VM_NAME".img \
				--os-type linux \
				--network bridge=br0,mac=$MAC_ADDR,model=virtio \
				--os-variant generic \
				--nographics
			#	--os-variant generic \
else
	echo -e "${YEL}sudo virt-install --import \
				--name $VM_NAME \
				--ram $RAM_SIZE \
                   		--vcpus $VCPU_NUMBER \
				--disk path=$DISK_PATH/"$VM_NAME".img \
				--os-type linux \
				--os-variant generic \
				--nographics"${NC}
	sudo virt-install  	--import \
				--name $VM_NAME \
				--ram $RAM_SIZE \
                   		--vcpus $VCPU_NUMBER \
				--disk path=$DISK_PATH/"$VM_NAME".img \
				--os-type linux \
				--os-variant generic \
				--nographics
fi


# Ask root password
if [ $DEBUG -eq 1 ]
then
	echo "##  Ask root password  ##"
fi
read -sp "$VM_NAME root password : " ROOT_PASSWORD
echo

# Ask home/bbicheron password
if [ $DEBUG -eq 1 ]
then
	echo "##  Ask bbicheron password  ##"
fi
read -sp "$VM_NAME bbicheron password : " USER_PASSWORD
echo

# TMP
USERNAME=bbicheron

# Create sysprep_file
echo "password ${USERNAME}:password:${USER_PASSWORD}
adduser ${USERNAME} sudo
adduser ${USERNAME} root
adduser ${USERNAME} adm" > sysprep_file

# Execute virt command
if [ $DEBUG -eq 1 ]
then
	echo "##  Apply new password to VM  ##"
fi

# Shutdown VM for modify file with guestfish
echo -e ${YEL}sudo virsh destroy $VM_NAME${NC}
sudo virsh destroy $VM_NAME

# Initialise the new VM with new parameters
echo -e ${YEL}sudo virt-sysprep -d $VM_NAME --commands-from-file sysprep_file --hostname $VM_NAME --root-password password:$ROOT_PASSWORD${NC} 
sudo virt-sysprep -d $VM_NAME --hostname $VM_NAME --root-password password:$ROOT_PASSWORD

# Set new VM to autostart
LIBVIRT_AUTOSTART="sudo virsh autostart $VM_NAME"
echo -e ${YEL}$LIBVIRT_AUTOSTART${NC}
eval $LIBVIRT_AUTOSTART

# Network interface for new VM MASTER
INTERFACE_M="# The loopback network interface
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
    address $IPv4_M
    netmask 255.255.255.0
    broadcast $BROADCAST_M
    post-up route add $GATEWAY_M dev eth0
    post-up route add default gw $GATEWAY_M
    pre-down route del $GATEWAY_M dev eth0
    pre-down route del default gw $GATEWAY_M"

# Define network interface for dhcp use
INTERFACE_DHCP="# The loopback network interface
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
iface eth0 inet6 auto"

# Put new network configuration file to VM
if [ "$NETWORK_TYPE" = "static" ]
then
	echo -e ${YEL}guestfish --rw -d $VM_NAME \
			-i write /etc/network/interfaces $INTERFACE_M ${NC}
	sudo guestfish --rw -d $VM_NAME \
			-i write /etc/network/interfaces "$INTERFACE_M"
else
	echo -e ${YEL}guestfish --rw -d $VM_NAME \
			-i write /etc/network/interfaces $INTERFACE_DHCP ${NC}
	sudo guestfish --rw -d $VM_NAME \
			-i write /etc/network/interfaces "$INTERFACE_DHCP"
fi

HOSTS_DENY="# /etc/hosts.deny: list of hosts that are _not_ allowed to access the system.
#                  See the manual pages hosts_access(5) and hosts_options(5).
#
# Example:    ALL: some.host.name, .some.domain
#             ALL EXCEPT in.fingerd: other.host.name, .other.domain
#
# If you're going to protect the portmapper use the name rpcbind for the
# daemon name. See rpcbind(8) and rpc.mountd(8) for further information.
#
# The PARANOID wildcard matches any host whose name does not match its
# address.
#
# You may wish to enable this to ensure any programs that don't
# validate looked up hostnames still leave understandable logs. In past
# versions of Debian this has been the default.
# ALL: PARANOID
ALL: ALL"

HOSTS_ALLOW_M="# /etc/hosts.allow: list of hosts that are allowed to access the system.
#                   See the manual pages hosts_access(5) and hosts_options(5).
#
# Example:    ALL: LOCAL @some_netgroup
#             ALL: .foobar.edu EXCEPT terminalserver.foobar.edu
#
# If you're going to protect the portmapper use the name rpcbind for the
# daemon name. See rpcbind(8) and rpc.mountd(8) for further information.
#
sshd: $IP_REBOND_VM: allow"

if [ "$NETWORK_TYPE" = "static" ]
then
	# Edit network VM Master file with new values
	echo -e ${YEL}guestfish --rw -d $VM_NAME \
			-i write /etc/hosts.deny hosts_deny${NC}
	echo -e ${YEL}guestfish --rw -d $VM_NAME \
			-i write /etc/hosts.allow hosts_allow${NC}
	sudo guestfish --rw -d $VM_NAME \
		-i write /etc/hosts.deny "$HOSTS_DENY"
	sudo guestfish --rw -d $VM_NAME \
		-i write /etc/hosts.allow "$HOSTS_ALLOW_M"

	# Change DNS to OVH DNS in /etc/resolv.conf
	echo -e "${YEL}guestfish --rw -d $VM_NAME \
	-i write /etc/resolv.conf nameserver 208.67.222.222
nameserver 208.67.222.220${NC}"

	sudo guestfish --rw -d $VM_NAME \
	-i write /etc/resolv.conf "nameserver 208.67.222.222
nameserver 208.67.222.220"
fi


# Start VM
echo -e ${YEL}sudo virsh start $VM_NAME${NC}
sudo virsh start $VM_NAME

# Connection to New VM and resize partition
if [ $DEFAULT_DISK_SIZE -lt $DISK_SIZE ]
then
	echo "Execute folowing commands:"
	echo "On host:"
	echo "sudo virsh shutdown $VM_NAME"
	sudo virsh shutdown $VM_NAME
	EXTEND_SIZE=$(( $DISK_SIZE - $DEFAULT_DISK_SIZE ))
	echo "sudo lvextend -L+${EXTEND_SIZE}G /dev/$GROUP_DISK/$DISK_NAME"
	sudo lvextend -L+${EXTEND_SIZE}G /dev/$GROUP_DISK/$DISK_NAME
	echo "On guest:"
	echo "fdisk -u /dev/sda3"
	echo "Create new primary disk with LINUX LVM type"
	echo "pvcreate /dev/sda3"
	echo "vgextend modele-vg /dev/sda3"
	echo lvextend -L+$EXTEND_SIZE /dev/modele-vg/home
	echo resize2fs /dev/modele-vg/home
fi
