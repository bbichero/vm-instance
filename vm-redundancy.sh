#!/bin/bash

# Define LVM path
LVM_PATH=/home/bbicheron

# Define IP of host for slave VM
IP_HOST2=<IPv4-host2>

# Define IP of VM (for send mail every day)
IP_VM_MAIL=<IPv4-mail-vm>

function exist()
{
	if [ $DEBUG -eq 1 ]
	then
		echo "## ENTER IN exist function test of $1 value ##"
	fi
	if [ -d "$1" ]
	then
    		echo "Error: Directory $1 already exists."
		return 0
	else
    		return 1
	fi
}

# Check LVM VARIABLE for executing them on Slave VM
if [ ${#LVM_DISK_CREATE} -eq 0 ]
then
	read -p 'VM name: ' VM_NAME
	read -p 'LVM disk name: ' DISK_NAME
	read -p 'LVM group name: ' GROUP_DISK
	read -p "LVM $DISK_NAME size: " DISK_SIZE
	read -p "VM path img (Default /$LVM_PATH/$GROUP_DISK/$DISK_NAME): " DISK_PATH
	if [ ${#DISK_PATH} -eq 0 ]
	then
		DISK_PATH="/$LVM_PATH/$GROUP_DISK/$DISK_NAME"
	else
		while exist "$DISK_PATH"
		do
			read -p 'VM path img: ' DISK_PATH
		done
	fi	
	LVM_DISK_CREATE="sudo lvcreate -L $DISK_SIZE""G -n $DISK_NAME $GROUP_DISK"
	LVM_DISK_PATH="mkdir $DISK_PATH"
	LVM_DISK_FORMAT="sudo mkfs -t ext4 /dev/$GROUP_DISK/$DISK_NAME"
	LVM_DISK_MOUNT="sudo mount -t ext4 /dev/$GROUP_DISK/$DISK_NAME /$LVM_PATH/$GROUP_DISK/$DISK_NAME"
	ADD_DISK_FSTAB="echo /dev/$GROUP_DISK/$DISK_NAME /$LVM_PATH/$GROUP_DISK/$DISK_NAME ext4 defaults 0 1 >> /etc/fstab"
fi
if [ ${#IPv4_M} -eq 0 ]
then
	read -p "IPv4 for $VM_NAME Master: " IPv4_M
fi

# Ask IP VM slave
read -p "$VM_NAME IPv4 slave : " IPv4_S

while [[ "$IPv4_M" == "$IPv4_S" ]]
do
	echo "IP Master and Slave must be distinct"
	read -p "$VM_NAME IPv4 slave : " IPv4_S
	while is_ipv4 "$IPv4_S"
	do
		read -p "$VM_NAME IPv4 slave : " IPv4_S
	done
done

# Ask MAC addr VM slave
read -p "$VM_NAME MAC addr master : " MAC_ADDR_S

while [[ "$MAC_ADDR_M" == "$MAC_ADDR_S" ]]
do
	echo "MAC addr Master and Slave must be distinct"
	read -p "$VM_NAME MAC addr slave : " MAC_ADDR_S
	while is_mac_addr "$MAC_ADDR_S"
	do
		read -p "$VM_NAME MAC addr slave : " MAC_ADDR_S
	done
done

# Ask gateway VM slave
read -p "$VM_NAME gateway slave : " GATEWAY_S

while [[ "$GATEWAY_M" == "$GATEWAY_S" ]]
do
	echo "Gateway Master and Slave must be distinct"
	read -p "$VM_NAME gateway slave : " GATEWAY_S
	while is_ipv4 $GATEWAY_S "gateway"
	do
		read -p "$VM_NAME gateway slave : " GATEWAY_S
	done
done

# Ask broadcast VM slave
read -p "$VM_NAME broadcast slave : " BROADCAST_S

while [[ "$BROADCAST_M" == "$BROADCAST_S" ]]
do
	echo "Broadcast Master and Slave must be distinct"
	read -p "$VM_NAME broadcast slave : " BROADCAST_S
	while is_ipv4 $BROADCAST_S "broadcast"
	do
		read -p "$VM_NAME broadcast slave : " BROADCAST_S
	done
done

# Network interface for new VM SLAVE
INTERFACE_S="# The loopback network interface
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
    address $IPv4_S
    netmask 255.255.255.0
    broadcast $BROADCAST_S
    post-up route add $GATEWAY_S dev eth0
    post-up route add default gw $GATEWAY_S
    pre-down route del $GATEWAY_S dev eth0
    pre-down route del default gw $GATEWAY_S"

# Make sure VM is not running
LIBVIRT_STOP_VM="sudo virsh destroy $VM_NAME"
eval $LIBVIRT_STOP_VM

# Edit network VM Master file with new values
echo sudo guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /etc/network/interfaces.conf "$INTERFACE_M"
root/libguestfs-1.36.1/run --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /etc/network/interfaces.conf "$INTERFACE_M"

# Execute all necessary command on remote host
echo sudo ssh root@"$IP_HOST2"
sudo ssh root@"$IP_HOST2" << EOF
$LVM_DISK_CREATE
$LVM_DISK_PATH
$LVM_DISK_FORMAT
$LVM_DISK_MOUNT
$ADD_DISK_FSTAB
EOF

# Copy XML VM file to other host
echo sudo scp /etc/libvirt/qemu/"$VM_NAME".xml root@"$IP_HOST2":/etc/libvirt/qemu/
sudo scp /etc/libvirt/qemu/"$VM_NAME".xml root@"$IP_HOST2":/etc/libvirt/qemu/

# Copy and past with dd virtual disk to other host
echo "sudo dd if=/dev/$GROUP_DISK/$DISK_NAME | pv -ptrb | ssh root@"$IP_HOST2"  dd of=/dev/$GROUP_DISK/$DISK_NAME bs=4M"
sudo dd if=/dev/$GROUP_DISK/$DISK_NAME | pv -ptrb | ssh root@"$IP_HOST2"  dd of=/dev/$GROUP_DISK/$DISK_NAME bs=4M

# Make slave VM autostart and stop it
echo sudo ssh root@"$IP_HOST2"
sudo ssh root@"$IP_HOST2" << EOF
service libvirtd restart
$LIBVIRT_AUTOSTART
$LIBVIRT_STOP_VM
EOF

HOSTS_DENY="# /etc/hosts.deny: list of hosts that are _not_ allowed to access the system.
#                  See the manual pages hosts_access(5) and hosts_options(5).
#
# Example:    ALL: some.host.name, .some.domain
#             ALL EXCEPT in.fingerd: other.host.name, .other.domain
#
# If you're going to protect the portmapper use the name rpcbind for the
# daemon name. See rpcbind(8) and rpc.mountd(8) for further information.
#
# The PARANOID wildcard matches any host whose name does not match its
# address.
#
# You may wish to enable this to ensure any programs that don't
# validate looked up hostnames still leave understandable logs. In past
# versions of Debian this has been the default.
# ALL: PARANOID
ALL: ALL"

HOSTS_ALLOW_S="# /etc/hosts.allow: list of hosts that are allowed to access the system.
#                   See the manual pages hosts_access(5) and hosts_options(5).
#
# Example:    ALL: LOCAL @some_netgroup
#             ALL: .foobar.edu EXCEPT terminalserver.foobar.edu
#
# If you're going to protect the portmapper use the name rpcbind for the
# daemon name. See rpcbind(8) and rpc.mountd(8) for further information.
#
sshd: $IPv4_M : allow"

# Add crontab to VM Master for rsync and log parsing
CRONTAB=$(<crontab_vm_m.txt)

echo guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /var/spool/cron/crontabs/root "$CRONTAB"
echo guestfish guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
 		-i chmod 0700 /var/spool/cron/crontabs/root
/root/libguestfs-1.36.1/run guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /var/spool/cron/crontabs/root "$CRONTAB"
/root/libguestfs-1.36.1/run guestfish guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
 		-i chmod 0700 /var/spool/cron/crontabs/root

# Generate backup.sh file and place it in /root/ of VM Master
BACKUP_FILE=$(<backup.txt)
BACKUP_FILE=$(sed "s/<ip-backup-vm>/$IPv4_S/g" <<< $RSYNC_FILE)

echo guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /root/backup.sh "$BACKUP_FILE"
echo guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i chmod 0700 /root/backup.sh
/root/libguestfs-1.36.1/run guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /root/backup.sh "$BACKUP_FILE"
/root/libguestfs-1.36.1/run guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i chmod 0700 /root/backup.sh

# Place contant of parse_rsync.sh file in /root
PARSE_RSYNC=$(</root/vm-creation/parse_rsync.sh)

echo  guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /root/parse_rsync.sh "$PARSE_RSYNC"
echo  guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i chmod 0700 /root/parse_rsync.sh
/root/libguestfs-1.36.1/run guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /root/parse_rsync.sh "$PARSE_RSYNC"
/root/libguestfs-1.36.1/run guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i chmod 0700 /root/parse_rsync.sh

# Start VM
echo virsh start $VM_NAME
virsh start $VM_NAME

# Generate rsync_VM.sh file and place it in VPS VM for send mail with resume
# log of rsync cron command execute every 10min on new VM
RSYNC_MAIL=$(<rsync_VM.txt)
RSYNC_MAIL=$(sed "s/VM_NAME=/VM_NAME=$VM_NAME/g" <<< "$RSYNC_MAIL")

echo sudo ssh root@"$IP_VM_MAIL"
sudo ssh root@"$IP_VM_MAIL" << EOF
echo "$RSYNC_MAIL" > /root/rsync_"$VM_NAME".sh
chmod 700 /root/rsync_"$VM_NAME".sh
echo "0  0  *  *  * /root/rsync_$VM_NAME.sh" > /var/spool/cron/crontabs/root
EOF

# Edit network VM Slave file with new values
# Dump XML qemu file, change MAC addr Slave and save change
sudo ssh root@"$IP_HOST2" << EOF
/root/libguestfs-1.36.1/run guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /etc/network/interfaces.conf "$INTERFACE_S"
/root/libguestfs-1.36.1/run guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /etc/hosts.deny $HOSTS_DENY
/root/libguestfs-1.36.1/run guestfish --rw -a "$DISK_PATH"/"$VM_NAME".img \
		-i write /etc/hosts.allow "$HOSTS_ALLOW_S"
virsh dumpxml $VM_NAME > "$VM_NAME".xml
sed -i -r "s/<mac address='([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})'\/>/<mac address='$MAC_ADDR_S'\/>/g" "$VM_NAME".xml
virsh create "$VM_NAME".xml
service libvirtd restart
virsh start $VM_NAME
EOF

echo "Ping test of $VM_NAME Slave: $IPv4_S"

