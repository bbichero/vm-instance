#!/bin/bash

# Defualt confi
LVM_PATH=/home/bbicheron

# Define color
YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Ask VM NAME
read -p 'VM Name you want to destroy: ' VM_NAME

while [ ${#VM_NAME} -eq 0 ]
do
	echo 'VM_NAME must be not NULL'
	read -p 'VM Name you want to destroy: ' VM_NAME
done

# Ask disk name
read -p 'LVM disk name: ' DISK_NAME
while [ ${#DISK_NAME} -eq 0 ]
do
	echo 'DISK NAME must be not NULL'
	read -p 'LVM disk name: ' DISK_NAME
done

# Ask group disk
read -p 'LVM group name: ' GROUP_DISK
while [ ${#GROUP_DISK} -eq 0 ]
do
	echo 'GROUP DISK must be not NULL'
	read -p 'LVM group name: ' GROUP_DISK
done

# Ask directory name, default /home/user/group-name/disk-name
read -p "VM path img (Default $LVM_PATH/$GROUP_DISK/$DISK_NAME): " DISK_PATH

if [ ${#DISK_PATH} -eq 0 ]
then
	DISK_PATH="$LVM_PATH/$GROUP_DISK/$DISK_NAME"
else
	while exist "$DISK_PATH"
	do
		read -p 'VM path img: ' DISK_PATH
	done
fi

# Destroy VM and undefine it
echo -e ${YEL}sudo virsh destroy $VM_NAME${NC}
sudo virsh destroy $VM_NAME
echo -e ${YEL}sudo virsh undefine $VM_NAME${NC}
sudo virsh undefine $VM_NAME

# Unmounting disk
echo -e ${YEL}sudo umount /dev/$GROUP_DISK/$DISK_NAME${NC}
sudo umount /dev/$GROUP_DISK/$DISK_NAME

# Remove logical disk
echo -e ${YEL}sudo lvremove /dev/$GROUP_DISK/$DISK_NAME${NC}
sudo lvremove /dev/$GROUP_DISK/$DISK_NAME

# Remove disk path
echo -e ${YEL}sudo rm -r $DISK_PATH${NC}
sudo rm -r $DISK_PATH

echo -e ${RED}"Don't forget to remove the line from /etc/fstab !!!"${NC}
echo -e ${GRE}VM completely remove from your system${NC}

